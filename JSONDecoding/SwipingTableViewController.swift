//
//  SwipingTableViewController.swift
//  JSONDecoding
//
//  Created by Sayed Mahmudul Alam on 9/4/18.
//  Copyright © 2018 sz. All rights reserved.
//

import UIKit

class SwipingViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let reusableID = "SliddingCell"
    
    let dataSource = [
        "Lorem ipsum dolor sit amet, pri alterum dolorem ad, iudico mollis repudiare ius no. Errem salutatus appellantur eum te. Duis nonumes assueverit est no, id his agam suscipiantur, nihil feugait usu an. ",
        "Lorem ipsum dolor sit amet, pri alterum dolorem ad, iudico mollis repudiare ius no. Errem salutatus appellantur eum te. Duis nonumes assueverit est no, id his agam suscipiantur, nihil feugait usu an. An duo atqui inani essent, dolorum accusam mei eu. Eos clita dolorum ad, liber accumsan ex eum, ex suas fuisset fierent cum. Vel no nihil assentior, mea in tale numquam.",
        "Lorem ipsum dolor sit amet, pri alterum dolorem ad, iudico mollis repudiare ius no. Errem salutatus appellantur eum te.",
        "Duis nonumes assueverit est no, id his agam suscipiantur, nihil feugait usu an. An duo atqui inani essent, dolorum accusam mei eu. Eos clita dolorum ad, liber accumsan ex eum, ex suas fuisset fierent cum.",
        "Ubique explicari vel et. Nostro corpora lobortis usu ut, dictas inermis expetendis at vis. Eu vim dolor inciderint, vide duis possim at qui. Usu dissentias theophrastus te, vim consul verterem ocurreret et, liber nominavi salutatus nam ea. Pri ei sale nobis comprehensam,",
        "Lorem ipsum dolor sit amet, pri alterum dolorem ad, iudico mollis repudiare ius no. Errem salutatus appellantur eum te. Duis nonumes assueverit est no, id his agam suscipiantur, nihil feugait usu an. An duo atqui inani essent, dolorum accusam mei eu. Eos clita dolorum ad, liber accumsan ex eum, ex suas fuisset fierent cum. Vel no nihil assentior, mea in tale numquam.",
        "Lorem ipsum dolor sit amet, pri alterum dolorem ad, iudico mollis repudiare ius no. Errem salutatus appellantur eum te.",
        "Duis nonumes assueverit est no, id his agam suscipiantur, nihil feugait usu an. An duo atqui inani essent, dolorum accusam mei eu. Eos clita dolorum ad, liber accumsan ex eum, ex suas fuisset fierent cum.",
        "Ubique explicari vel et. Nostro corpora lobortis usu ut, dictas inermis expetendis at vis. Eu vim dolor inciderint, vide duis possim at qui. Usu dissentias theophrastus te, vim consul verterem ocurreret et, liber nominavi salutatus nam ea. Pri ei sale nobis comprehensam,"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        let cellNib = UINib(nibName: "SampleTableViewCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: reusableID)
        
        dynamicCellHeight()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
}

extension SwipingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableID, for: indexPath) as! SampleTableViewCell
        cell.mainLabel.text = dataSource[indexPath.row]
        configureScrollView(cell)
        return cell
    }
    
    func configureScrollView(_ cell: SampleTableViewCell) {
        cell.delegate = self
        cell.selectionStyle = .none
        cell.mainView.translatesAutoresizingMaskIntoConstraints = false
        cell.sideView.translatesAutoresizingMaskIntoConstraints = false
        cell.containerView.translatesAutoresizingMaskIntoConstraints = false
        
        cell.mainView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        cell.sideView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        
        cell.containerView.widthAnchor.constraint(equalToConstant: view.frame.width*2)
        cell.scrollView.contentSize = CGSize(width: view.frame.width*2, height: cell.mainView.frame.height)
        
        
        cell.scrollView.isUserInteractionEnabled = false
        cell.contentView.addGestureRecognizer(cell.scrollView.panGestureRecognizer)
    }
    
    func dynamicCellHeight() {
        tableView.estimatedRowHeight = 128
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(dataSource[indexPath.row])
    }
}

extension SwipingViewController: SampleTableViewCellDelegate {
    func upperLeftButtonAction(cell: SampleTableViewCell) {
        print("upper left button pressed")
    }
    
    func bottomLeftButtonAction(cell: SampleTableViewCell) {
        print("bottom left button pressed")
    }
    
    func bottomRightButtonAction(cell: SampleTableViewCell) {
        print("bottom right button pressed")
    }
    
    func upperRightButtonAction(cell: SampleTableViewCell) {
        print("upper right button pressed")
    }
    
    
}
