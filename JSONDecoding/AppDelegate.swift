//
//  AppDelegate.swift
//  JSONDecoding
//
//  Created by Sayed Mahmudul Alam on 3/25/18.
//  Copyright © 2018 sz. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
}

