//
//  SampleTableViewCell.swift
//  JSONDecoding
//
//  Created by Sayed Mahmudul Alam on 9/4/18.
//  Copyright © 2018 sz. All rights reserved.
//

import UIKit

protocol SampleTableViewCellDelegate {
    func upperLeftButtonAction(cell: SampleTableViewCell)
    func bottomLeftButtonAction(cell: SampleTableViewCell)
    func bottomRightButtonAction(cell: SampleTableViewCell)
    func upperRightButtonAction(cell: SampleTableViewCell)
}


class SampleTableViewCell: UITableViewCell {
    
    var delegate: SampleTableViewCellDelegate?

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var upperLeftButton: UIButton!
    @IBOutlet weak var bottomLeftButton: UIButton!
    @IBOutlet weak var bottomRightButton: UIButton!
    
    @IBOutlet weak var upperRightButton: UIButton!
    
    @IBAction func upperLeftButtonAction(_ sender: UIButton) {
        delegate?.upperLeftButtonAction(cell: self)
    }
    @IBAction func bottomLeftButtonAction(_ sender: UIButton) {
        delegate?.bottomLeftButtonAction(cell: self)
    }
    
    @IBAction func bottomRightButtonAction(_ sender: UIButton) {
        delegate?.bottomRightButtonAction(cell: self)
    }
    
    @IBAction func upperRightButtonAction(_ sender: UIButton) {
        delegate?.upperRightButtonAction(cell: self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
//        print("From tableview cell: \(view)")
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
